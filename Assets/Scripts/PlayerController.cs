﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
 
  //Скорость игрока
  [Header("Player Speed")]
  [Range(0, 8)]
  [SerializeField] float pl_speed;
  //Сила прыжка игрока
  [Header("Player Jump Force")]
  [Range(5, 15)]
  [SerializeField] float pl_jumpforce;
  //Дистанцыя до низа. Нужна для Raycast'а
  [Header("Distance Down")]
  [SerializeField] float disDown;
  //косается ли земли
  bool isGround;
  //Повернут ли вправо
  bool isRight;
  //Компонент физики игрока
  private Rigidbody2D rb;
  //Иницыализацыя переменных

	void Start () {
		rb = GetComponent<Rigidbody2D>();
    isRight = true;
	}
  //Каждый кадр проверяется столкновение с землей
	
	void Update () {
    CheckGround();
	}
  //Каждый кадр проверяет нажали клавиша для ходбы или прыжка

  void FixedUpdate() {
    if(Input.GetButtonDown("Jump") && isGround) Jump();
    if(Input.GetButton("Horizontal")) Move();
  }

  //Метод отвечающий за движение

  void Move() {
    Vector2 curPos = transform.position;
    Vector2 dir = Input.GetAxis("Horizontal") * Vector2.right;
    transform.position = Vector2.MoveTowards(curPos, dir + curPos, pl_speed * Time.deltaTime);
    Revert();
  }

  //Метод отвечающий за прыжок

  void Jump() {
    //rb.AddForce(Vector2.up * pl_jumpforce, ForceMode2D.Impulse);
    rb.velocity = Vector2.up * pl_jumpforce;
  }

  //Повороты

  void Revert() {
    if ( (isRight && Input.GetAxis("Horizontal") < 0) || (!isRight && Input.GetAxis("Horizontal") > 0) ) {
      GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
      isRight = !isRight;
    }
  }

  //Проверка столкновения с землей
  void CheckGround()
  {
    RaycastHit2D[] rch = Physics2D.RaycastAll(transform.position, Vector2.down, disDown);
    isGround = rch.Length > 1;
  }
}
