﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

  //Цель камеры
  [SerializeField] Transform target;
  //Скорость передвежения камеры
  [SerializeField] float speed;


	void Update () {
    CamMove();
	}
  //Движение камеры
  void CamMove() {
    Vector3 targetPos = target.position;
    targetPos.z = -15;
    transform.position = Vector3.MoveTowards(transform.position, targetPos, speed);
  }
}
